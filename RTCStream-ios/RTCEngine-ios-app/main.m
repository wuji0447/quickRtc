//
//  main.m
//  RTCEngine-ios-app
//
//  Created by yafei zhang on 2019/3/27.
//  Copyright © 2019 RTCEngine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
