//
//  AppDelegate.h
//  RTCEngine-ios-app
//
//  Created by yafei zhang on 2019/3/27.
//  Copyright © 2019 RTCEngine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

