Pod::Spec.new do |ss|
ss.name             = 'RTCEngine_osx'
ss.version          = '1.0.1'
ss.summary          = 'dotEngine  realtime audio/video  as a service'


ss.description      = <<-DESC
dotEngine realtime audio/video as a service
DESC

ss.homepage         = 'https://dot.cc'
ss.license          = { :type => 'MIT' }
ss.author           = { 'notedit' => 'notedit@gmail.com' }
ss.source           = { :git => 'https://gitee.com/wuji0447/quickRtc.git', :tag => ss.version.to_s }

ss.preserve_paths = 'RTCEngine_osx.framework'
#s.vendored_libraries = 'RTCEngine_osx.framework'
ss.vendored_frameworks = 'RTCEngine_osx.framework'


#ss.framework = 'osx'

#ss.osx.framework = 'Accelerate', 'SystemConfiguration','CoreWLAN', 'Foundation', 'CoreAudio', 'CoreMedia', 'VideoToolbox', 'AudioToolbox', 'CFNetwork', 'CoreGraphics', 'CoreVideo'
ss.libraries = 'c', 'stdc++'
ss.requires_arc = true
ss.dependency  'AFNetworking'
ss.dependency  'AgoraRtcEngine_macOS','~> 2.2.0.0'
ss.dependency  'JWT'

end
